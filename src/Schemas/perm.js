const mongoose = require('mongoose');

const perm = mongoose.Schema({
    guildId: { type: String },
    perm: { type: String },
    users: { type: Object },
    roles: { type: Object }
})

module.exports = mongoose.model('perm', perm);
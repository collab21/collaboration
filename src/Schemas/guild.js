const mongoose = require('mongoose');

const guild = mongoose.Schema({
    guildId: { type: String },
    language: { type: String },
    prefix: { type: String },
    messageDelete: { type: Boolean },
    messageDeleteId: { type: String }
})

module.exports = mongoose.model('guild', guild);
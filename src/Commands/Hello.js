const Command = require('../Structures/Command');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['hallo']
		});
	}

    async run(message, ...args) {

		let channel = message.getChannel();
		const newChannel = channel[0]

		message.channel.send({ content: `Hey: ${newChannel}` });
    }
}
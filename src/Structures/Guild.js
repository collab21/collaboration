const { Guild } = require('discord.js');
const guildSchema = require('../Schemas/guild');

module.exports = Object.defineProperties(Guild.prototype, {
	fetchSettings: {
		value: async function() {
			this.settings = await guildSchema.findOne({ guildId: this.id })           
            return this.settings;
		},
	},
    setSettings: {
        value: async function(settings) {
            this.client.utils.log(`Guild: [${this.id}] updated settings: ${Object.keys(settings)}`)

            if (this.settings.guildId) {
                await guildSchema.findOneAndUpdate({ guildId: this.id }, settings)
            } 
        }
    },
    settings: {
		value: {},
		writable: true,
	},
    language: {
        value: false,
        writeable: true
    },
    prefix: {
        value: false,
        writeable: true
    }
})
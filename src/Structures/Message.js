const { Message } = require('discord.js')

module.exports = Object.defineProperties(Message.prototype, {
	args: {
		value: [],
		writable: true,
	},
	// Get channel(s) from message (via ID or mention)
	getChannel: {
		value: function() {
			const channels = [];
			// get all channels mentioned
			for (let i = 0; i < this.args.length; i++) {
				if ([...this.mentions.channels.values()][i] || this.guild.channels.cache.get(this.args[i])) {
					channels.push([...this.mentions.channels.values()][i] || this.guild.channels.cache.get(this.args[i]));
				}
			}
			channels.push(this.channel);
			return channels;
		},
	},
});
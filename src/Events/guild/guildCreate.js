const Event = require('../../Structures/Event');
const guildSchema = require('../../Schemas/guild');

module.exports = class extends Event {
    constructor(...args) {
        super(...args, {

        })
    }

    async run(guild) {
        const language = 'en'
        const prefix = 'p!'

        await guild.fetchSettings();

        if (!guild.settings) {
            await guildSchema.create({
                guildId: guild.id,
                language: language,
                prefix: prefix
            })
        }
    }
}
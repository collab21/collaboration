const Event = require('../Structures/Event');
const mongoose = require('mongoose');

module.exports = class extends Event {

    constructor(...args) {
        super(...args, {
            once: true
        })
    }

    async run() {

        await mongoose.connect("mongodb+srv://LukasWeber:vh5iRdtCgyW6JbR3@lixun.q9gis.mongodb.net/test", {
            useNewURlParser: true,
            useUnifiedTopology: true
        })

        console.log(`Collab Is Now Online!`)

        this.client.guilds.cache.forEach(async (guild) => {
            await guild.fetchSettings();
        })

        this.client.user.setActivity(`${this.client.prefix}help | We are in development!`)
        this.client.user.setStatus('dnd')
    }
}
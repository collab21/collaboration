const Discord = require('discord.js');
const Event = require('../../Structures/Event');
const guild = require('../../Schemas/guild');

module.exports = class extends Event {
    async run(message) {

        if (message.guild) {

            const mentionRegex = RegExp(`^<@!?${this.client.user.id}>$`); 
            const mentionRegexPrefix = RegExp(`^<@!?${this.client.user.id}> `);    
 
            if (message.author.bot) return

            const data = await guild.findOne({ guildId: message.guild.id });

            if (message.content.match(mentionRegex)) message.reply({ content: `Hello ${message.author.username}, The bots prefix is ${data.prefix}` })

            const prefix = message.content.match(mentionRegexPrefix) ? message.content.match(messageRegexPrefix)[0] : data.prefix;

            if (!message.content.startsWith(prefix)) return;

            const [cmd, ...args] = message.content.slice(prefix.length).trim().split(/ +/g);

            const command = this.client.commands.get(cmd.toLowerCase()) || this.client.commands.get(this.client.aliases.get(cmd.toLowerCase()));

            if (command) {
                console.log(`Command ${command.name} was just ran`)

                if (command.ownerOnly === true && message.author.id != '438756132937269248') {
                    return message.reply({ content: `This Command Is Restricted For The Developers.` })
                }

                await message.guild.fetchSettings();

                await command.run(message, ...args);
            }
        }
    }
}
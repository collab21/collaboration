// Imports

const Perspective = require('./Structures/Perspective');
const config = require('../config.json');
const i18n = require('i18n')
const path = require('path')
require('./Structures')

// Main Code - Runs Bot

const client = new Perspective(config)

client.start()

i18n.configure({
    loacles: ['en', 'de'],
    directory: path.join(__dirname, 'Languages'),
})

i18n.setLocale('en')